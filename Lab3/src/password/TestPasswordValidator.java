package password;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestPasswordValidator {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	//length tests/////////////////////////////////////////////////////////////////
	
	@Test
	public void testCheckPasswordLength() {
		assertTrue("Invalid password length",PasswordValidator.checkPasswordLength("thisisavalidpassword"));
	}
	@Test
	public void testCheckPasswordLengthBoundryIn() {
		assertTrue("Invalid password length",PasswordValidator.checkPasswordLength("testtest"));
		
	}
	@Test
	public void testCheckPasswordLengthBoundryOut() {
		assertFalse("Invalid password length",PasswordValidator.checkPasswordLength("tooshrt"));
	}
	@Test 
	public void testCheckPasswordLengthException()  {
		assertFalse("Invalid password length",PasswordValidator.checkPasswordLength(""));
	}
	
	
	//digit tests//////////////////////////////////////////////////////////////////////////////////
	
	@Test
	public void testCheckPasswordDigits() {
		assertTrue("Invalid Number of Digits",PasswordValidator.checkPasswordDigits("th1s1isav4l1dpassword"));
	}
	@Test
	public void testCheckPasswordDigitsBoundaryIn() {
		assertTrue("Invalid number of Digits",PasswordValidator.checkPasswordDigits("th1isw1llwork"));
		
	}
	@Test
	public void testCheckPasswordDigitsBoundaryOut() {
		assertFalse("Invalid number of Digits",PasswordValidator.checkPasswordDigits("w0ntwork"));
	}
	@Test
	public void testCheckPasswordDigitException() {
		assertFalse("Invalid number of Digits",PasswordValidator.checkPasswordDigits(""));
	}

}
