package password;

public class PasswordValidator {
	
	public static void main(String[] args) {
		
	}
	
	public static boolean checkPasswordLength(String password) {
		
		return password.length()>=8;
//refactored for efficiency
//		if(password.length()>=8)
//			return true;
//		return false;
	}
	public static boolean checkPasswordDigits(String password) {
		int numOfDigits=0;
		for(int j=0;j<password.length();j++) {
			if(numOfDigits>=2)//returns true as soon as 2 digits are found no need to continue
				return true;
			if(Character.isDigit(password.charAt(j)))
				numOfDigits++;
		}
		return false;//has not found 2 or more digits so return false
		
	}

}
